﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMARDump
{
    enum DMAR_Types : ushort
    {
        ACPI_DMAR_TYPE_HARDWARE_UNIT = 0,
        ACPI_DMAR_TYPE_RESERVED_MEMORY = 1,
        ACPI_DMAR_TYPE_ROOT_ATS = 2,
        ACPI_DMAR_TYPE_HARDWARE_AFFINITY = 3,
        ACPI_DMAR_TYPE_NAMESPACE = 4
    }

    enum DMAR_Scope_Types : byte
    {
        ACPI_DMAR_SCOPE_TYPE_NOT_USED = 0,
        ACPI_DMAR_SCOPE_TYPE_ENDPOINT = 1,
        ACPI_DMAR_SCOPE_TYPE_BRIDGE = 2,
        ACPI_DMAR_SCOPE_TYPE_IOAPIC = 3,
        ACPI_DMAR_SCOPE_TYPE_HPET = 4,
        ACPI_DMAR_SCOPE_TYPE_NAMESPACE = 5
    }
}
