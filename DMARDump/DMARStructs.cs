﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMARDump
{
    class ACPI_Table_Header
    {
        public string Signature;
        public UInt32 Length;
        public byte Revision;
        public byte Checksum;
        public string OemId;
        public string OemTableId;
        public UInt32 OemRevision;
        public string AslCompilerId;
        public UInt32 AslCompilerRevision;
    }

    class ACPI_Table_DMAR
    {
        public byte Width;
        public byte Flags;
        public byte[] Reserved;
    }

    class ACPI_DMAR_Header
    {
        public DMAR_Types Type;
        public UInt16 Length;
    }

    class ACPI_DMAR_Hardware_Unit
    {
        public byte Flags;
        public byte Reserved;
        public UInt16 Segment;
        public UInt64 Address;
    }

    class ACPI_DMAR_ReservedMemory
    {
        public UInt16 Reserved;
        public UInt16 Segment;
        public UInt64 BaseAddress;
        public UInt64 EndAddress;
    }

    class ACPI_DMAR_RootATS
    {
        public byte Flags;
        public byte Reserved;
        public UInt16 Segment;
    }

    class ACPI_DMAR_HardwareAffinity
    {
        public UInt32 Reserved;
        public UInt64 BaseAddress;
        public UInt32 ProximityDomain;
    }

    class ACPI_DMAR_Namespace
    {
        public byte Reserved1;
        public byte Reserved2;
        public byte Reserved3;
        public byte DeviceNumber;
        public byte DeviceName;
    }

    class ACPI_DMAR_Device_Scope
    {
        public DMAR_Scope_Types ScopeType;
        public byte Length;
        public UInt16 Reserved;
        public byte EnumerationId;
        public byte Bus;
    }

    class ACPI_DMAR_PCI_Path
    {
        public byte Device;
        public byte Function;
    }
}
