﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMARDump
{
    class Program
    {
        /***
         * 
         * DMA Remapping Table (DMAR) Dumper
         * Prints information about the DMAR, which can be extracted from a running system using NirSoft FirmwareTablesView or other tools.
         * 
         * A large amount of this code was derived from the Linux source code, particularly:
         * - http://lxr.free-electrons.com/source/include/acpi/actbl2.h (line 395 to 524)
         * - http://lxr.free-electrons.com/source/include/acpi/actypes.h#L383
         * - http://lxr.free-electrons.com/source/drivers/iommu/dmar.c
         * 
         * This has been thoroughly tested on... my box (Intel i7-4970K, Z97 chipset, VT-x and VT-d enabled)
         * Please test on other systems and file new issues if anything is wrong/broken.
         * 
         ***/

        const int ACPI_NAME_SIZE = 4;
        const int ACPI_OEM_ID_SIZE = 6;
        const int ACPI_OEM_TABLE_ID_SIZE = 8;
        const int ACPI_TABLE_HEADER_SIZE = ACPI_NAME_SIZE + 4 + 1 + 1 + ACPI_OEM_ID_SIZE + ACPI_OEM_TABLE_ID_SIZE + 4 + ACPI_NAME_SIZE + 4;
        const int ACPI_DMAR_TABLE_RESERVED_SIZE = 10;
        const int ACPI_DMAR_TABLE_SIZE = 2 + ACPI_DMAR_TABLE_RESERVED_SIZE;
        const int ACPI_DMAR_HEADER_SIZE = 4;
        const int ACPI_DMAR_SCOPE_SIZE = 6;
        const int ACPI_DMAR_PCI_PATH_SIZE = 2;

        static string GetVersionString()
        {
            var asm = System.Reflection.Assembly.GetExecutingAssembly();
            var fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(asm.Location);
            return string.Format("{0}.{1}", fvi.FileMajorPart, fvi.FileMinorPart);
        }

        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("DMA Remapping Table Dumper (DMARDump) - v{0}", GetVersionString());
                Console.WriteLine("Written by Graham \"gsuberland\" Sutherland");
                Console.WriteLine("https://bitbucket.org/gsuberland/dmardump/");
                Console.WriteLine();
                Console.WriteLine("Usage: dmardump.exe [dmar.bin]");
                return;
            }

            // attempt to read the file
            byte[] dmarData = null;
            try
            {
                dmarData = File.ReadAllBytes(args[0]);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[ERROR] Failed to open file: {0}", ex.Message);
                return;
            }

            // check that the data size is appropriate
            if (dmarData.Length < ACPI_TABLE_HEADER_SIZE + ACPI_DMAR_TABLE_SIZE)
            {
                Console.WriteLine("[ERROR] Unexpected end of file - too short to be a real DMAR table!");
                return;
            }

            using (var dmar = new BinaryReader(new MemoryStream(dmarData)))
            {
                // first dump the standard ACPI header
                var acpiHeader = new ACPI_Table_Header();

                acpiHeader.Signature = Encoding.ASCII.GetString(dmar.ReadBytes(ACPI_NAME_SIZE));

                if (acpiHeader.Signature != "DMAR")
                {
                    Console.WriteLine("[ERROR] Not a DMAR dump, ACPI table header signature incorrect.");
                    return;
                }

                acpiHeader.Length = dmar.ReadUInt32();
                acpiHeader.Revision = dmar.ReadByte();
                acpiHeader.Checksum = dmar.ReadByte();
                acpiHeader.OemId = Encoding.ASCII.GetString(dmar.ReadBytes(ACPI_OEM_ID_SIZE));
                acpiHeader.OemTableId = Encoding.ASCII.GetString(dmar.ReadBytes(ACPI_OEM_TABLE_ID_SIZE));
                acpiHeader.OemRevision = dmar.ReadUInt32();
                acpiHeader.AslCompilerId = Encoding.ASCII.GetString(dmar.ReadBytes(ACPI_NAME_SIZE));
                acpiHeader.AslCompilerRevision = dmar.ReadUInt32();

                Console.WriteLine("======= ACPI Header =======");
                Console.WriteLine("Signature:        {0}",     acpiHeader.Signature);
                Console.WriteLine("Length:           {0:X8}h", acpiHeader.Length);
                Console.WriteLine("Revision:         {0:X2}h", acpiHeader.Revision);
                Console.WriteLine("Checksum:         {0:X2}h", acpiHeader.Checksum);
                Console.WriteLine("OEM ID:           {0}", acpiHeader.OemId);
                Console.WriteLine("OEM Table ID:     {0}", acpiHeader.OemTableId);
                Console.WriteLine("OEM Revision:     {0:X8}h", acpiHeader.OemRevision);
                Console.WriteLine("ASL Compiler ID:  {0}", acpiHeader.AslCompilerId);
                Console.WriteLine("ASL Compiler Rev: {0:X8}h", acpiHeader.AslCompilerRevision);
                Console.WriteLine();

                // dump the DMAR table header
                var dmarTable = new ACPI_Table_DMAR();

                dmarTable.Width = dmar.ReadByte();
                dmarTable.Flags = dmar.ReadByte();
                dmarTable.Reserved = dmar.ReadBytes(ACPI_DMAR_TABLE_RESERVED_SIZE);

                Console.WriteLine("======= DMAR Header =======");
                Console.WriteLine("Width:    {0:X2}h", dmarTable.Width);
                Console.WriteLine("Flags:    {0:X2}h", dmarTable.Flags);
                Console.WriteLine("Reserved: {0}", string.Join(" ", dmarTable.Reserved.Select(v => string.Format("{0:X2}", v))));
                Console.WriteLine();

                // iterate entries
                while (dmar.BaseStream.Position < dmar.BaseStream.Length - 4)
                {
                    var header = new ACPI_DMAR_Header();
                    header.Type = (DMAR_Types)dmar.ReadUInt16();
                    header.Length = dmar.ReadUInt16();

                    Console.WriteLine("========== Entry ==========");

                    // save stream position prior to parsing
                    long savedPos = dmar.BaseStream.Position;
                    // compute the position of the next entry
                    long nextPos = (savedPos - ACPI_DMAR_HEADER_SIZE) + header.Length;
                    switch (header.Type)
                    {
                        case DMAR_Types.ACPI_DMAR_TYPE_HARDWARE_UNIT:
                            Console.WriteLine("Type:     HARDWARE_UNIT");
                            Console.WriteLine("Length:   {0:X4}h", header.Length);
                            ParseHardwareUnitEntry(dmar);
                            Console.WriteLine();
                            ParseScopes(dmar, nextPos);
                            break;
                        case DMAR_Types.ACPI_DMAR_TYPE_RESERVED_MEMORY:
                            Console.WriteLine("Type:        RESERVED_MEMORY");
                            Console.WriteLine("Length:      {0:X4}h", header.Length);
                            ParseReservedMemoryEntry(dmar);
                            Console.WriteLine();
                            ParseScopes(dmar, nextPos);
                            break;
                        case DMAR_Types.ACPI_DMAR_TYPE_ROOT_ATS:
                            Console.WriteLine("Type:     ROOT_ATS");
                            Console.WriteLine("Length:   {0:X4}h", header.Length);
                            ParseRootATSEntry(dmar);
                            Console.WriteLine();
                            ParseScopes(dmar, nextPos);
                            break;
                        case DMAR_Types.ACPI_DMAR_TYPE_HARDWARE_AFFINITY:
                            Console.WriteLine("Type:            HARDWARE_AFFINITY");
                            Console.WriteLine("Length:          {0:X4}h", header.Length);
                            ParseHardwareAffinityEntry(dmar);
                            Console.WriteLine();
                            ParseScopes(dmar, nextPos);
                            break;
                        default:
                            Console.WriteLine("Type:   UNKNOWN ({0:X4}h)", (UInt16)header.Type);
                            Console.WriteLine("Length: {0:X4}h", header.Length);
                            break;
                    }
                    Console.WriteLine();
                    // restore stream position
                    dmar.BaseStream.Seek(savedPos, SeekOrigin.Begin);

                    // check that we're not gonna skip off the end or something
                    int skipDistance = header.Length - ACPI_DMAR_HEADER_SIZE;
                    if (skipDistance < 4 || /* bad skip distance? */
                        (dmar.BaseStream.Position + skipDistance > dmar.BaseStream.Length - ACPI_DMAR_HEADER_SIZE)) /* or skipping far enough that we're past the end / can't fit another DMAR header in? */
                    {
                        break;
                    }
                    else
                    {
                        // jump past this entry to the next one
                        dmar.BaseStream.Seek(skipDistance, SeekOrigin.Current);
                    }
                }

#if DEBUG
                Console.ReadLine();
#endif
            }
        }

        private static void ParseHardwareUnitEntry(BinaryReader dmar)
        {
            var hardwareUnit = new ACPI_DMAR_Hardware_Unit();

            hardwareUnit.Flags = dmar.ReadByte();
            hardwareUnit.Reserved = dmar.ReadByte();
            hardwareUnit.Segment = dmar.ReadUInt16();
            hardwareUnit.Address = dmar.ReadUInt64();

            Console.WriteLine("Flags:    {0:X2}h", hardwareUnit.Flags);
            Console.WriteLine("Reserved: {0:X2}h", hardwareUnit.Reserved);
            Console.WriteLine("Segment:  {0:X4}h", hardwareUnit.Segment);
            Console.WriteLine("Address:  {0:X16}h", hardwareUnit.Address);
        }

        private static void ParseReservedMemoryEntry(BinaryReader dmar)
        {
            var reservedMem = new ACPI_DMAR_ReservedMemory();

            reservedMem.Reserved = dmar.ReadUInt16();
            reservedMem.Segment = dmar.ReadUInt16();
            reservedMem.BaseAddress = dmar.ReadUInt64();
            reservedMem.EndAddress = dmar.ReadUInt64();

            Console.WriteLine("Reserved:    {0:X4}h", reservedMem.Reserved);
            Console.WriteLine("Segment:     {0:X4}h", reservedMem.Segment);
            Console.WriteLine("BaseAddress: {0:X16}h", reservedMem.BaseAddress);
            Console.WriteLine("EndAddress:  {0:X16}h", reservedMem.EndAddress);
        }

        private static void ParseRootATSEntry(BinaryReader dmar)
        {
            var rootATS = new ACPI_DMAR_RootATS();

            rootATS.Flags = dmar.ReadByte();
            rootATS.Reserved = dmar.ReadByte();
            rootATS.Segment = dmar.ReadUInt16();

            Console.WriteLine("Flags:    {0:X2}h", rootATS.Flags);
            Console.WriteLine("Reserved: {0:X2}h", rootATS.Reserved);
            Console.WriteLine("Segment:  {0:X4}h", rootATS.Segment);
        }

        public static void ParseHardwareAffinityEntry(BinaryReader dmar)
        {
            var hardwareAffinity = new ACPI_DMAR_HardwareAffinity();

            hardwareAffinity.Reserved = dmar.ReadUInt32();
            hardwareAffinity.BaseAddress = dmar.ReadUInt64();
            hardwareAffinity.ProximityDomain = dmar.ReadUInt32();

            Console.WriteLine("Reserved:        {0:X8}h", hardwareAffinity.Reserved);
            Console.WriteLine("BaseAddress:     {0:X16}h", hardwareAffinity.BaseAddress);
            Console.WriteLine("ProximityDomain: {0:X8}h", hardwareAffinity.ProximityDomain);
        }

        public static void ParseNamespaceEntry(BinaryReader dmar)
        {
            var nameSpace = new ACPI_DMAR_Namespace();

            nameSpace.Reserved1 = dmar.ReadByte();
            nameSpace.Reserved2 = dmar.ReadByte();
            nameSpace.Reserved3 = dmar.ReadByte();
            nameSpace.DeviceNumber = dmar.ReadByte();
            nameSpace.DeviceName = dmar.ReadByte();

            Console.WriteLine("Reserved:     {0:X2} {1:X2} {2:X2}", nameSpace.Reserved1, nameSpace.Reserved2, nameSpace.Reserved3);
            Console.WriteLine("DeviceNumber: {0:X2}h", nameSpace.DeviceNumber);
            Console.WriteLine("DeviceName:   {0:X2}h", nameSpace.DeviceName);
        }

        public static void ParseScopes(BinaryReader dmar, long nextPos)
        {
            while (dmar.BaseStream.Position < nextPos)
            {
                var scope = new ACPI_DMAR_Device_Scope();

                long savedPos = dmar.BaseStream.Position;

                scope.ScopeType = (DMAR_Scope_Types)dmar.ReadByte();
                scope.Length = dmar.ReadByte();

                if (scope.Length < ACPI_DMAR_SCOPE_SIZE)
                {
                    Console.WriteLine("[ERROR] Invalid scope size.");
                    return;
                }

                scope.Reserved = dmar.ReadUInt16();
                scope.EnumerationId = dmar.ReadByte();
                scope.Bus = dmar.ReadByte();

                Console.WriteLine("> Scope:");
                Console.WriteLine(">> Type:           {0}", scope.ScopeType.ToString());
                Console.WriteLine(">> Length:         {0:X2}h", scope.Length);
                Console.WriteLine(">> Reserved:       {0:X4}h", scope.Reserved);
                Console.WriteLine(">> Enumeration ID: {0:X2}h", scope.EnumerationId);
                Console.WriteLine(">> Bus:            {0:X2}h", scope.Bus);

                if (scope.Length >= ACPI_DMAR_SCOPE_SIZE + ACPI_DMAR_PCI_PATH_SIZE)
                {
                    var pciPath = new ACPI_DMAR_PCI_Path();

                    pciPath.Device = dmar.ReadByte();
                    pciPath.Function = dmar.ReadByte();

                    Console.WriteLine(">> PCI Device      {0:X2}h", pciPath.Device);
                    Console.WriteLine(">> PCI Function    {0:X2}h", pciPath.Function);
                }

                Console.WriteLine();

                dmar.BaseStream.Seek(savedPos + scope.Length, SeekOrigin.Begin);
            }
        }
    }
}
