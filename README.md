# DMARDump - DMA Remapping Table Dumper #

DMARDump prints information about the DMA Remapping (DMAR) table, which can be extracted from the ACPI tables using [FirmwareTablesView](http://www.nirsoft.net/utils/firmware_tables_view.html) or a similar tool. The DMAR is present in the ACPI tables when IOMMU is enabled, which is also known as virtualised I/O direct access (VT-d).

It has been tested "extensively" on two systems:

* i7-4790K, Z97 chipset.
* Xeon E3 1245-v3, C226 chipset (thanks to Daniel Wright)

I would be grateful if people could test on other platforms that have VT-d support.

### What's all this then? ###

It's just a C# tool that parses data from a dumped DMAR table. Extract the DMAR using a tool of your choice, then point this tool at it, like this:

```
#!

dmardump.exe c:\path\to\dmar.bin
```

Here's an example output:

```
======= ACPI Header =======
Signature:        DMAR
Length:           00000080h
Revision:         01h
Checksum:         61h
OEM ID:           INTEL
OEM Table ID:     BDW
OEM Revision:     00000001h
ASL Compiler ID:  INTL
ASL Compiler Rev: 00000001h

======= DMAR Header =======
Width:    26h
Flags:    03h
Reserved: 00 00 00 00 00 00 00 00 00 00

========== Entry ==========
Type:     HARDWARE_UNIT
Length:   0020h
Flags:    01h
Reserved: 00h
Segment:  0000h
Address:  00000000FED90000h

> Scope:
>> Type:           ACPI_DMAR_SCOPE_TYPE_IOAPIC
>> Length:         08h
>> Reserved:       0000h
>> Enumeration ID: 08h
>> Bus:            F0h
>> PCI Device      1Fh
>> PCI Function    00h

> Scope:
>> Type:           ACPI_DMAR_SCOPE_TYPE_HPET
>> Length:         08h
>> Reserved:       0000h
>> Enumeration ID: 00h
>> Bus:            F0h
>> PCI Device      0Fh
>> PCI Function    00h


========== Entry ==========
Type:        RESERVED_MEMORY
Length:      0030h
Reserved:    0000h
Segment:     0000h
BaseAddress: 00000000DDD58000h
EndAddress:  00000000DDD65FFFh

> Scope:
>> Type:           ACPI_DMAR_SCOPE_TYPE_ENDPOINT
>> Length:         08h
>> Reserved:       0000h
>> Enumeration ID: 00h
>> Bus:            00h
>> PCI Device      1Dh
>> PCI Function    00h

> Scope:
>> Type:           ACPI_DMAR_SCOPE_TYPE_ENDPOINT
>> Length:         08h
>> Reserved:       0000h
>> Enumeration ID: 00h
>> Bus:            00h
>> PCI Device      1Ah
>> PCI Function    00h

> Scope:
>> Type:           ACPI_DMAR_SCOPE_TYPE_ENDPOINT
>> Length:         08h
>> Reserved:       0000h
>> Enumeration ID: 00h
>> Bus:            00h
>> PCI Device      14h
>> PCI Function    00h
```

Kinda cool, right?

### What's the point? ###

I dunno. I was bored and talking to some people on #r_netsec about detecting VT-d support on x86, and found that the best way to do it is to find the DMAR entry in the ACPI tables. So I wrote this tool to extract data from it, because I don't think there's anything out there that does it.

### Contribution guidelines ###

* Pull requests accepted.
* Please verify that your code doesn't break DMAR dumps from other platforms.
* Please comment thoroughly.

### License ###

* This project is released under [MIT License](https://en.wikipedia.org/wiki/MIT_License). Go nuts.